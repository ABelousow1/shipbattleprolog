
#field is list of lists filled with E variable by default
field_row(0,L,_):- L = [],!.
field_row(N,L,E):- N1 is N-1, field_row(N1,L1,E), L=[E|L1].

field(N, L, Paint):- field_row(N,L1,Paint), field_row(N,L, L1).

#drawing predicate
draw1(_,1).

#painting cell by calling P callable variable
paint(P,0,0, [[Val|T1]|T], [[Pres|T1]|T]):-call(P,Val,Pres),!.

paint(P,0,N, [[H|T]|T1], L2):-
    N1 is N-1, paint(P,0,N1,[T|T1],[Lb|T1]), L2 = [[H|Lb]|T1].

paint(P,N,M,[H|T], Lo):-
    N1 is N-1,
    paint(P,N1,M,T,Lo2),
    Lo = [H|Lo2].

#ship horizontal placement
ship_hor(1,N,M,Li,Lo):-paint(draw1,N,M,Li,Lo),!.

ship_hor(K,N,M,Li,Lo):-
    K1 is K-1,
    M1 is M+1,
    ship_hor(K1,N,M1,Li,Lo1),
    paint(draw1,N,M,Lo1,Lo2),
    Lo = Lo2.

#ship vertical placement
ship_ver(1,N,M,Li,Lo):-paint(draw1,N,M,Li,Lo),!.

ship_ver(K,N,M,Li,Lo):-
    K1 is K-1,
    N1 is N+1,
    ship_ver(K1,N1,M,Li,Lo1),
    paint(1,N,M,Lo1,Lo2),
    Lo = Lo2.

#field printing
print_matrix([]).
print_matrix([H|T]) :- write(H), nl, print_matrix(T).

#shot predicate (TODO)
shot_ship(X,Res):-X=1->Res=1;Res=0.

shot(X, Y, Four,Fenemy):-paint(shot_ship, X,Y, Fenemy).
